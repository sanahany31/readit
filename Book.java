package application;

import java.util.Objects;

import com.mysql.cj.jdbc.Blob;

import javafx.scene.image.Image;

public class Book {
	private int bookID;	
	private String bookName;
	private String bookGenre;
	private String bookAuthorName;
	private String bookPublicationDate;	
	private String bookBrief;	
	private Blob Img;
	
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Book(String bookName, String bookGenre, String bookAuthorName, String bookPublicationDate, String bookBrief,
			Blob img) {
		super();
		this.bookName = bookName;
		this.bookGenre = bookGenre;
		this.bookAuthorName = bookAuthorName;
		this.bookPublicationDate = bookPublicationDate;
		this.bookBrief = bookBrief;
		Img = img;
	}
	public String getBookName() {
		return bookName;
	}
	public void setBookName(String bookName) {
		this.bookName = bookName;
	}
	public String getBookGenre() {
		return bookGenre;
	}
	public void setBookGenre(String bookGenre) {
		this.bookGenre = bookGenre;
	}
	public String getBookAuthorName() {
		return bookAuthorName;
	}
	public void setBookAuthorName(String bookAuthorName) {
		this.bookAuthorName = bookAuthorName;
	}
	public String getBookPublicationDate() {
		return bookPublicationDate;
	}
	public void setBookPublicationDate(String bookPublicationDate) {
		this.bookPublicationDate = bookPublicationDate;
	}
	public String getBookBrief() {
		return bookBrief;
	}
	public void setBookBrief(String bookBrief) {
		this.bookBrief = bookBrief;
	}
	public Blob getImg() {
		return Img;
	}
	public void setImg(Blob img) {
		Img = img;
	}
	@Override
	public int hashCode() {
		return Objects.hash(Img, bookAuthorName, bookBrief, bookGenre, bookID, bookName, bookPublicationDate);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Book other = (Book) obj;
		return Objects.equals(Img, other.Img) && Objects.equals(bookAuthorName, other.bookAuthorName)
				&& Objects.equals(bookBrief, other.bookBrief) && Objects.equals(bookGenre, other.bookGenre)
				&& bookID == other.bookID && Objects.equals(bookName, other.bookName)
				&& Objects.equals(bookPublicationDate, other.bookPublicationDate);
	}
	@Override
	public String toString() {
		return "Book [bookID=" + bookID + ", bookName=" + bookName + ", bookGenre=" + bookGenre + ", bookAuthorName="
				+ bookAuthorName + ", bookPublicationDate=" + bookPublicationDate + ", bookBrief=" + bookBrief
				+ ", Img=" + Img + "]";
	}
	
}
