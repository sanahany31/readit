package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Welcome_Controller implements Initializable{
	@FXML
	private Button Login_Button;

	@FXML
	private Button SignUp_Button;
	
	

	public void openHome(String windowName, int width, int height) {
    	try {
			Stage primaryStage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource(windowName));
			Scene scene = new Scene(root,width,height);
			scene.getStylesheets().add(getClass().getResource("styleLibrary.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} 
		catch(IOException e1) {
			e1.printStackTrace();
		}
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		Login_Button.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				openHome("Log_In.fxml",600,400);
				Stage stage = (Stage) Login_Button.getScene().getWindow();
			    stage.close();	
				}
				
			
		});
			
		SignUp_Button.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				openHome("Sign_Up.fxml",600,561);
				Stage stage = (Stage) SignUp_Button.getScene().getWindow();
				stage.close();	
			}
			
		});
		
	}
}
