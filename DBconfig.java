package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBconfig {
	
	static Connection con = null;
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/readit";
			con = DriverManager.getConnection(url,"sana","sana123");
			System.out.println("connected");
		}
		catch(ClassNotFoundException | SQLException e) {	
			e.printStackTrace();
		}
	return con;
	}
}
