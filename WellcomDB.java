package application;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class WellcomDB {

	@FXML
    private Label st;
	Connection conn = DBconfig.getConnection(); 

	public void statusDB(ActionEvent e) throws IOException, SQLException{
		if(!conn.isClosed()) {
			st.setText("Connected");
		}
		else {
			st.setText("Not Connected");
		}

	}

}
