package application;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Sign_Up_Controller implements Initializable{
	@FXML
    private TextField addressText;

    @FXML
    private TextField bioText;

    @FXML
    private Button btnSignup;
    @FXML
    private Button btnBack;

    @FXML
    private PasswordField  cpasswordText;

    @FXML
    private TextField emailText;

    @FXML
    private TextField fnameText;
    @FXML
    private PasswordField  passwordText;
    @FXML
    private TextField snameText;
    @FXML
    private TextField usernameText;
    @FXML
    private Label error;
    @FXML
    private Label fnameLB;
    @FXML
    private Label snameLB;
    @FXML
    private Label cpasswordLB;
    @FXML
    private Label usernameLB;
    @FXML
    private Label emailLB;
    @FXML
    private Label passwordLB;
    @FXML
    private Label addressLB;
    @FXML
    private Label bioLB;

    public boolean isFilled() {
    	if(!fnameText.getText().isEmpty() && !snameText.getText().isEmpty() && !emailText.getText().isEmpty() && !usernameText.getText().isEmpty() && !passwordText.getText().isEmpty() && !emailText.getText().isEmpty() && !cpasswordText.getText().isEmpty() && !addressText.getText().isEmpty()) {
    		
    		return true;
    	}
    	else {
    		error.setText("All required fields must be filled out");
    		return false;
    	}
    }
    
    public boolean checkFname() {
    	int option = 0;
    	if(!fnameText.getText().matches("[a-zA-Z]+")) {
    		option=1; //The first name must contains only letters
    	}
    	else if(fnameText.getText().length()>20) {
    		option =2; //The first name length cannot be greater than 20
    	}
    		
    	switch(option) {
    		case 1:    	fnameLB.setText("Name must contains only letters");
    		return false;
    		case 2:    	fnameLB.setText("Name length cannot be greater than 20");
    		return false;
    	}
    	return true;
    }
    public boolean checkSname() {
    	int option = 0;
    	if(!snameText.getText().matches("[a-zA-Z]+")) {
    		option=1; //The first name must contains only letters
    	}
    	else if(snameText.getText().length()>20) {
    		option =2; //The first name length cannot be greater than 20
    	}
    	
    	switch(option) {
	    	case 1:    	snameLB.setText("Name must contains only letters");
	    	return false;
	    	case 2:    	snameLB.setText("Name length cannot be greater than 20");
	    	return false;
    	}
    	return true;
    }
    public boolean checkUsername() {
    	int option = 0;
    	if(!usernameText.getText().matches("^[A-Za-z][A-Za-z0-9_-]*$")) {
    		option=1; //The first name must contains only letters
    	}
    	/*else if(usernameText.getText().equals(addressLB))*/
    	else if(usernameText.getText().length()>10 || usernameText.getText().length()>20) {
    		option =2; //The first name length cannot be greater than 20
    	}
    	else if(UsersDB.checkUsername(usernameText.getText())) {
    		option = 3;
    	}
    	
    	switch(option) {
    	case 1:    	usernameLB.setText("Username can only contain letters,\n numbers and '_' and starts with letter.");
    	return false;
    	case 2:    	usernameLB.setText("Username can only contain 10 characters \nto 20 characters.");
    	return false;
    	case 3:    	usernameLB.setText("This username is taken, try another");
    	return false;
    	}
    	return true;
    }

    public boolean checkEmail() {
        boolean flag;
    	String regexPattern = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";
        Pattern pattern = Pattern.compile(regexPattern); 
        Matcher matcher = pattern.matcher(emailText.getText());
        flag = matcher.matches(); 
        if(!flag) {
        	emailLB.setText("Invalid Email");
        	return false;
        }
        return true;

    }
    
    public boolean checkPassword() {
    	if(passwordText.getText().length()<=15) {
    		return true;
    	}
    	else {
    		passwordLB.setText("Passwords is too long.");
    		return false;
    	}
    }
    public boolean confirmPassword() {
    	if(passwordText.getText().equals(cpasswordText.getText())) {
    		return true;
    	}
    	else {
    		cpasswordLB.setText("Passwords does not match");
    		return false;
    	}
    }
    public boolean checkAddress() {
    	if(addressText.getText().length()<=20) {
    		return true;
    	}
    	else {
    		addressLB.setText("Passwords is too long.");
    		return false;
    	}
    }
    public boolean checkBio() {
    	if(bioText.getText().length()<=30) {
    		return true;
    	}
    	else {
    		bioLB.setText("Passwords is too long.");
    		return false;
    	}
    }
    public boolean execute_getSignUpData() throws SQLException {
    	if(isFilled()) {
			Users user = new Users(fnameText.getText().trim(),snameText.getText().trim()
					,emailText.getText().trim(),usernameText.getText().trim(),passwordText.getText().trim(),
					addressText.getText().trim(),bioText.getText().trim());
	    	if(UsersDB.addUser(user)) {	
				return true;
			}
			else {
				System.out.println("Error occured. Try again.");
				return false;
			}
    	}
    	else {
    		return false;
    	}
    
	}
    public void openHome(String windowName, int width, int height) {
    	try {
			Stage primaryStage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource(windowName));
			Scene scene = new Scene(root,width,height);
			scene.getStylesheets().add(getClass().getResource("styleLibrary.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} 
		catch(IOException e1) {
			e1.printStackTrace();
		}
	}
    
    @Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		btnSignup.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				try {
					fnameLB.setText("");
					snameLB.setText("");
					emailLB.setText("");
					usernameLB.setText("");
					passwordLB.setText("");
					cpasswordLB.setText("");
					addressLB.setText("");
					bioLB.setText("");
					error.setText("");
					
					if(isFilled() && checkFname() && checkSname() && checkEmail()&&checkUsername() && checkPassword() && confirmPassword() && checkAddress() && execute_getSignUpData()) {
						openHome("Home.fxml",1245,679);	
						Stage stage = (Stage) btnSignup.getScene().getWindow();
					    stage.close();	
					}
					
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnBack.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				openHome("Welcome.fxml",600,400);	
				Stage stage = (Stage) btnBack.getScene().getWindow();
			    stage.close();	

			}
		});	
		
}
}