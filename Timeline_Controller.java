package application;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Timeline_Controller implements Initializable{
	 @FXML
	 private HBox cardLayout;
	 @FXML
	 private Button btnBack;
	 @FXML
    private Button btnChat;

    @FXML
    private Button btnCustomerS;

    @FXML
    private Button btnHome;

    @FXML
    private Button btnProfile;

    @FXML
    private Button btnTimeline;
	 public void openHome(String windowName, int width, int height) {
	    	try {
				Stage primaryStage = new Stage();
				Parent root = FXMLLoader.load(getClass().getResource(windowName));
				Scene scene = new Scene(root,width,height);
				scene.getStylesheets().add(getClass().getResource("styleLibrary.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();
			} 
			catch(IOException e1) {
				e1.printStackTrace();
			}
	    	
		}
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		btnBack.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				openHome("Welcome.fxml",600,400);	
				Stage stage = (Stage) btnBack.getScene().getWindow();
			    stage.close();	
			}
		});
		btnHome.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				openHome("Home.fxml",1245,679);	
				Stage stage = (Stage) btnHome.getScene().getWindow();
			    stage.close();	

			}
		});
		btnProfile.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				try {
					FXMLLoader loader = new FXMLLoader(getClass().getResource("Profile.fxml"));
					Parent root = (Parent) loader.load();
					//profile.display(Log_In_Controller.uname);
					Stage stage = new Stage();
					root.getStylesheets().add(getClass().getResource("styleLibrary.css").toExternalForm());

					stage.setScene(new Scene(root));
					stage.show();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Stage stage = (Stage) btnProfile.getScene().getWindow();
			    stage.close();	
				
			}
		});
		btnTimeline.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				openHome("Timeline.fxml",1245,679);	
				Stage stage = (Stage) btnTimeline.getScene().getWindow();
				stage.close();	
				
			}
		});
		
	}
	

}
