package application;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDB {
	static Connection conn = DBconfig.getConnection();
	
	public static List<Book> getBookCardData(){
		List <Book> book_list = new ArrayList <Book>();
		try {
			String sql = "SELECT  `bookName`, `bookGenre`, `bookAuthorName` FROM `books` WHERE 1";
			
			PreparedStatement praparedStatement =(PreparedStatement)conn.prepareStatement(sql); 
			ResultSet resultSet = praparedStatement.executeQuery();
			while(resultSet.next()) {
				Book book = new Book();
				book.setBookName(resultSet.getString(1));
				book.setBookGenre(resultSet.getString(2));
				book.setBookAuthorName(resultSet.getString(3));
			}
			conn.close();
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return book_list;
	}

}
