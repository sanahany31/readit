package application;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UsersDB {
	static Connection conn = DBconfig.getConnection();
	
	public static List<Users> getUseres(){
		List <Users> user_list = new ArrayList <Users>();
		try {
			String sql = "SELECT * FROM `users` WHERE 1";
			
			PreparedStatement praparedStatement =(PreparedStatement)conn.prepareStatement(sql); 
			ResultSet resultSet = praparedStatement.executeQuery();
			while(resultSet.next()) {
				Users user = new Users(resultSet.getString(2),resultSet.getString(3),resultSet.getString(4),resultSet.getString(5),resultSet.getString(6),resultSet.getString(7),resultSet.getString(8));	
				user_list.add(user);
			}
			conn.close();
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return user_list;
	}
	public static boolean checkLogin(String username, String password) {
		try {
			String sql = "SELECT * FROM `users` WHERE `userUserName`=? AND `userPassword`=?";
			
			PreparedStatement praparedStatement =(PreparedStatement)conn.prepareStatement(sql); 
			praparedStatement.setString(1,username );
			praparedStatement.setString(2,password );
			ResultSet resultSet = praparedStatement.executeQuery();
			while(resultSet.next()) {
				return true;
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return false;
	}
	public static boolean checkUsername(String username) {
		try {
			String sql = "SELECT * FROM `users` WHERE `userUserName`=?";
			
			PreparedStatement praparedStatement =(PreparedStatement)conn.prepareStatement(sql); 
			praparedStatement.setString(1,username );
			ResultSet resultSet = praparedStatement.executeQuery();
			while(resultSet.next()) {
				return true;
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return false;
	}
	
	
	public static boolean addUser(Users u) throws SQLException {
		//if(u.getUserFirstName().isEmpty() && u.getUserSecondName().isEmpty() &&u.getUserEmail().isEmpty() && u.getUserUserName().isEmpty() && u.getUserPassword().isEmpty() && u.getUserAddress()&&u.getUserBio().isEmpty()) {
		String sql = "INSERT INTO `users`(`userFirstName`, `userSecondName`, `userEmail`, `userUserName`, `userPassword`, `userAddress`, `userBio`) VALUES ('"+u.getUserFirstName()+"','"+u.getUserSecondName()+"','"+u.getUserEmail()+"','"+u.getUserUserName()+"','"+u.getUserPassword()+"','"+u.getUserAddress()+"','"+u.getUserBio()+"')";
		Connection conn = DBconfig.getConnection();
		Statement stmt = conn.createStatement();
		int x = stmt.executeUpdate(sql);
		
		conn.close();
		if(x>0) {
			return true;
		}
		return false;
	}
}
