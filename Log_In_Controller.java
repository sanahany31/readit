package application;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class Log_In_Controller implements Initializable{
	@FXML
	private TextField passwordTextF;
	@FXML
    private TextField userNameTextF;
    @FXML
    private Label error;
    @FXML
    private Button btnLogin;
    @FXML
    private Button btnBack;

    public String errorMessage="";
    public static String uname;
    
    public boolean isFilled() {
    	boolean isFilled = true;
    	if(userNameTextF.getText().toLowerCase().isEmpty()) {
    		isFilled= false;
    		errorMessage = "Please enter your username!";
    	}
    	if(passwordTextF.getText().isEmpty()) {
    		isFilled= false;
    		if(errorMessage.isEmpty()) {
        		errorMessage = "Please enter your password!";
    		}
    		else {
        		errorMessage +="\nPlease enter your password!";
    		}
    	}
		error.setText(errorMessage);
    	return isFilled;
    }
	
    public boolean login2() {
		if(UsersDB.checkLogin(userNameTextF.getText().trim().toLowerCase(), passwordTextF.getText())) {
			uname = userNameTextF.getText();
			return true;
		}
		else {		
			error.setText("Username or password is invalid!");
			return false;
		}    	
    }
    public void openHome(String windowName, int width, int height) {
    	try {
			Stage primaryStage = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource(windowName));
			Scene scene = new Scene(root,width,height);
			scene.getStylesheets().add(getClass().getResource("styleLibrary.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} 
		catch(IOException e1) {
			e1.printStackTrace();
		}
    	
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		btnLogin.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				errorMessage="";
				if(isFilled() && login2()) {
					//openHome("Home.fxml",1254,679);
					try {
						FXMLLoader loader = new FXMLLoader(getClass().getResource("Home.fxml"));
						Parent root = (Parent) loader.load();
						Home_Controller home = loader.getController();
						home.display(uname);
						Stage stage = new Stage();
						root.getStylesheets().add(getClass().getResource("styleLibrary.css").toExternalForm());
						stage.setScene(new Scene(root));
						stage.show();
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					Stage stage = (Stage) btnLogin.getScene().getWindow();
				    stage.close();
					

				}
				
			}
		});
		btnBack.setOnMouseClicked(new EventHandler <MouseEvent>(){
			@Override
			public void handle(MouseEvent event) {
				openHome("Welcome.fxml",600,400);	
				Stage stage = (Stage) btnBack.getScene().getWindow();
			    stage.close();	

			}
		});
			
	}
}
