package application;

import java.util.Objects;

public class Users {
	private int userID;
	private String userFirstName;
	private String userSecondName;
	private String userEmail;
	private String userUserName;
	private String userPassword;
	private String userAddress;
	private String userBio;
	public Users(String string, String string2, String string3, String string4, String string5, String string6, String string7,String string8) {
		super();
		// TODO Auto-generated constructor stub
	}
	public Users(String userFirstName, String userSecondName, String userEmail, String userUserName, String userPassword,
			String userAddress, String userBio) {
		super();
		this.userFirstName = userFirstName;
		this.userSecondName = userSecondName;
		this.userEmail = userEmail;
		this.userUserName = userUserName;
		this.userPassword = userPassword;
		this.userAddress = userAddress;
		this.userBio = userBio;
	}
	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	public String getUserFirstName() {
		return userFirstName;
	}
	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}
	public String getUserSecondName() {
		return userSecondName;
	}
	public void setUserSecondName(String userSecondName) {
		this.userSecondName = userSecondName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserUserName() {
		return userUserName;
	}
	public void setUserUserName(String userUserName) {
		this.userUserName = userUserName;
	}

	public String getUserPassword() {
		return userPassword;
	}
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	public String getUserAddress() {
		return userAddress;
	}
	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}
	public String getUserBio() {
		return userBio;
	}
	public void setUserBio(String userBio) {
		this.userBio = userBio;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(userAddress, userBio,  userEmail,userUserName, userFirstName, userID, userPassword,
				userSecondName);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		return Objects.equals(userAddress, other.userAddress) && Objects.equals(userBio, other.userBio)
				&& Objects.equals(userEmail, other.userEmail)
				&& Objects.equals(userUserName, other.userUserName)
				&& Objects.equals(userFirstName, other.userFirstName) && userID == other.userID
				&& Objects.equals(userPassword, other.userPassword)
				&& Objects.equals(userSecondName, other.userSecondName);
	}
	@Override
	public String toString() {
		return "Users [userID=" + userID + ", userFirstName=" + userFirstName + ", userSecondName=" + userSecondName
				+ ", userEmail=" + userEmail + ", userPassword=" + userPassword +", userUserName="+ userUserName+ ", userAddress=" + userAddress
				+ ", userBio=" + userBio + "]";
	}
	
}
